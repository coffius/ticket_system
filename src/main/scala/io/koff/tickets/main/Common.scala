package io.koff.tickets.main

import cats.Show
import cats.syntax.show._
import cats.effect.IO
import io.circe.Decoder
import io.circe.parser.decode

import scala.io.Source

/**
  * Commons logic of working with files and console
  */
object Common {

  /**
    * Extracts the first element from the iterator and wraps it in `IO[..]`.
    * Creates `IO[..]` with `IllegalStateException` if the iterator is empty
    */
  def getFirst[T](iter: Iterator[T]): IO[T] = {
    if(iter.hasNext) {
      IO.pure(iter.next())
    } else {
      IO.raiseError(new IllegalStateException("the iterator is empty"))
    }
  }

  /**
    * Reads the first line from the file as a string
    * @param filename a name of a file to read from
    */
  def readFirstLine(filename: String): IO[String] = {
    val fileSource = IO(Source.fromFile(filename))
    for {
      file <- fileSource
      lines = file.getLines()
      firstLine <- getFirst(lines).handleErrorWith { e =>
        IO.raiseError(new IllegalArgumentException(s"file[$filename] is empty", e))
      }
    } yield {
      firstLine
    }
  }

  /**
    * Reads one Json object as `T` from the file
    * @param filename a name of a file to read from
    */
  def readObjFromFile[T: Decoder](filename: String): IO[T] = for {
    line <- readFirstLine(filename)
    objOrErr = decode[T](line)
    obj <- IO.fromEither(objOrErr)
  } yield {
    obj
  }

  /**
    * Prints all the elements of `values` to the console
    * @param values values to print
    */
  def printAll[T: Show](values: Traversable[T]): IO[Unit] = IO {
    values.foreach { value =>
      println(value.show)
    }
  }
}
