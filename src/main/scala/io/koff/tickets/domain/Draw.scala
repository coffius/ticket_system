package io.koff.tickets.domain

import io.circe.Decoder
import io.circe.DecodingFailure.fromThrowable
import io.koff.tickets.collections.LimitedSet
import io.koff.tickets.collections.LimitedSet.SetError
import io.koff.tickets.domain.numbers.{RegularNumber, StarNumber}

import scala.collection.Set
import scala.collection.SortedSet

/**
  * Represents a draw result
  */
case class Draw(
  regular: LimitedSet[RegularNumber],
  star: LimitedSet[StarNumber],
)

object Draw {
  case class DrawError(msg: String, setError: SetError) extends Exception(msg)

  def create(
    regular: Set[RegularNumber],
    star: Set[StarNumber]
  ): Either[DrawError, Draw] = for {
    regularSet <- LimitedSet.build(5, 5, regular).left.map(regularErr)
    starSet <- LimitedSet.build(2, 2, star).left.map(starErr)
  } yield {
    Draw(regularSet, starSet)
  }

  private def regularErr(setErr: SetError): DrawError = {
    DrawError(s"problem while creating regular set: $setErr", setErr)
  }

  private def starErr(setErr: SetError): DrawError = {
    DrawError(s"problem while creating star set: $setErr", setErr)
  }

  implicit val Decoder: Decoder[Draw] = c => {
    for {
      regular <- c.downField("regular").as[SortedSet[RegularNumber]]
      star <- c.downField("star").as[SortedSet[StarNumber]]
      draw <- create(regular, star).left.map(fromThrowable(_, c.history))
    } yield {
      draw
    }
  }
}
