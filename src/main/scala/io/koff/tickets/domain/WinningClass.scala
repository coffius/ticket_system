package io.koff.tickets.domain

import akka.stream.scaladsl.{Flow, Keep, Sink}
import cats.instances.all._
import cats.kernel.Monoid

import scala.collection.SortedSet
import scala.concurrent.Future

/**
  * Prize level
  * @param coolness defines how cool the level is.
  *                 And its order related to other levels)
  * @param correctRegular defines how many correct regular numbers must be in a ticket
  * @param correctStar defines how many correct star numbers must be in a ticket
  */
case class WinningClass(coolness: Int, correctRegular: Int, correctStar: Int)

object WinningClass {
  implicit val winningClassOrdering: Ordering[WinningClass] = Ordering.by { wc: WinningClass =>
    wc.coolness
  }

  val Class1:  WinningClass = WinningClass(coolness = 1,  correctRegular = 5, correctStar = 2)
  val Class2:  WinningClass = WinningClass(coolness = 2,  correctRegular = 5, correctStar = 1)
  val Class3:  WinningClass = WinningClass(coolness = 3,  correctRegular = 5, correctStar = 0)
  val Class4:  WinningClass = WinningClass(coolness = 4,  correctRegular = 4, correctStar = 2)
  val Class5:  WinningClass = WinningClass(coolness = 5,  correctRegular = 4, correctStar = 1)
  val Class6:  WinningClass = WinningClass(coolness = 6,  correctRegular = 4, correctStar = 0)
  val Class7:  WinningClass = WinningClass(coolness = 7,  correctRegular = 3, correctStar = 2)
  val Class8:  WinningClass = WinningClass(coolness = 8,  correctRegular = 2, correctStar = 2)
  val Class9:  WinningClass = WinningClass(coolness = 9,  correctRegular = 3, correctStar = 1)
  val Class10: WinningClass = WinningClass(coolness = 10, correctRegular = 3, correctStar = 0)
  val Class11: WinningClass = WinningClass(coolness = 11, correctRegular = 1, correctStar = 2)
  val Class12: WinningClass = WinningClass(coolness = 12, correctRegular = 2, correctStar = 1)
  val Class13: WinningClass = WinningClass(coolness = 13, correctRegular = 2, correctStar = 0)

  val All: SortedSet[WinningClass] = SortedSet(
    Class1,
    Class2,
    Class3,
    Class4,
    Class5,
    Class6,
    Class7,
    Class8,
    Class9,
    Class10,
    Class11,
    Class12,
    Class13
  )

  /**
    * Find the coolest winning class for the ticket in the draw.
    * @param draw draw numbers
    * @param ticket the ticket to check
    * @param classes available winning classes
    * @return the coolest winning class if possible
    */
  def findWinningClass(
    draw: Draw,
    ticket: Ticket,
    classes: SortedSet[WinningClass]
  ): Option[WinningClass] = {
    val correctRegular = ticket.regular.values.intersect(draw.regular.values)
    val correctStar = ticket.star.values.intersect(draw.star.values)
    classes.find { winningClass =>
      correctRegular.size >= winningClass.correctRegular && correctStar.size >= winningClass.correctStar
    }
  }

  private val monoid = Monoid[Map[WinningClass, Int]]

  /**
    * Creates a sink to accumulate a number of winning tickets per class
    * @param draw draw numbers
    * @param classes available winning classes
    */
  def calculateWins(
    draw: Draw,
    classes: SortedSet[WinningClass]
  ): Sink[Ticket, Future[Map[WinningClass, Int]]] = {
    val wins = Flow[Ticket]
      // find a particular winning class if exists
      .map(findWinningClass(draw, _, classes))
      // Filter out cases when a winning class has not been found
      .collect { case Some(win) => win }
      // prepare for `Monoid.combine(...)`
      .map(wc => Map(wc -> 1))

    val counter = Sink.fold(monoid.empty)(monoid.combine)
    wins.toMat(counter)(Keep.right)
  }
}