package io.koff.tickets.domain

import cats.syntax.applicativeError._
import cats.instances.future._
import io.circe.syntax._
import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import akka.stream.scaladsl.{FileIO, Keep, Source}
import akka.util.ByteString
import org.scalacheck.Gen.Parameters
import org.scalacheck.rng.Seed
import io.koff.tickets.json.TicketFormat._

import scala.concurrent.duration._
import scala.concurrent.Await

object TicketGenerator {
  private val TicketNumber = 1000
  private val Filename = Paths.get("task_3_tickets.txt")
  private implicit val system: ActorSystem = ActorSystem("TicketGenerator")
  private implicit val mat: Materializer = ActorMaterializer()
  import system.dispatcher

  def main(args: Array[String]): Unit = {
    val gen = Gens.TicketGens.correctTicket
    val fileSink = FileIO.toPath(Filename)
    val graph =
      Source
        .unfold(()) { _ =>
          val ticket = gen.pureApply(Parameters.default, Seed.random())
          Some((), ticket)
        }
        .take(TicketNumber)
        .map(_.asJson.noSpaces + "\n")
        .map(ByteString(_))
        .toMat(fileSink)(Keep.right)

    val asyncResult = graph.run().attempt
    val result = Await.result(asyncResult, 30.seconds)
    println(s"generation result: $result")

    Await.result(system.terminate(), 60.seconds)
  }
}
