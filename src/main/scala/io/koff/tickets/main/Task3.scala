package io.koff.tickets.main

import java.nio.file.Paths

import akka.actor.ActorSystem
import akka.stream.scaladsl.{FileIO, Framing, Keep}
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.ByteString
import cats.Show
import cats.effect.{ExitCode, IO, IOApp}
import cats.syntax.functor._
import io.circe.parser._
import io.koff.tickets.domain.{Draw, Ticket, WinningClass}
import io.koff.tickets.json.TicketFormat._

import scala.collection.SortedMap

/**
  * Calculates how many ticket have won in each winning class.
  */
object Task3 extends IOApp {
  private val FileWithTickets = "task_3_tickets.txt"
  private val FileWithDraw = "task_3_draw.json"
  private val Delimiter = "\n"

  private implicit val system: ActorSystem = ActorSystem("TicketGenerator")
  private implicit val mat: Materializer = ActorMaterializer()
  private implicit val ResultShow: Show[(WinningClass, Int)] = Show.show {
    case (win, count) => s"Winning class ${win.coolness} = $count"
  }

  private def processWinningClasses(draw: Draw, fileWithTickets: String): IO[SortedMap[WinningClass, Int]] = {
    val fileSource = FileIO.fromPath(Paths.get(fileWithTickets))
    val calcSink = WinningClass.calculateWins(draw, WinningClass.All)
    val tickets = fileSource
      .via(Framing.delimiter(ByteString(Delimiter), 1024))
      .map(_.utf8String)
      .map(decode[Ticket])
      .collect {
        case Right(ticket) => ticket
      }

    val graph = tickets.toMat(calcSink)(Keep.right)
    val asyncResult = graph.run()
    IO.fromFuture(IO(asyncResult)).map(res => SortedMap.empty[WinningClass, Int] ++ res)
  }

  private def terminateSystem(as: ActorSystem): IO[Unit] = {
    IO.fromFuture(IO(as.terminate())).void
  }

  override def run(args: List[String]): IO[ExitCode] = for {
    draw <- Common.readObjFromFile[Draw](FileWithDraw)
    winCounts <- processWinningClasses(draw, FileWithTickets)
    _ <- Common.printAll(winCounts)
    _ <- terminateSystem(system)
  } yield {
    ExitCode.Success
  }
}
