package io.koff.tickets.domain.numbers

import io.koff.tickets.domain.{TicketNumber, TicketNumberBuilder}

/**
  * Value class for star numbers in tickets
  */
case class StarNumber private (value: Int) extends AnyVal

object StarNumber extends TicketNumberBuilder[StarNumber] {
  override val minValue: Int = 1
  override val maxValue: Int = 5
  override protected def create(value: Int): StarNumber = StarNumber(value)
  override implicit val ticketNumber: TicketNumber[StarNumber] = ticketNumber => ticketNumber.value
}


