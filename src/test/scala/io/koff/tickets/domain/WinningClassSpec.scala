package io.koff.tickets.domain

import io.koff.tickets.UnitSpec
import io.koff.tickets.collections.LimitedSet
import io.koff.tickets.domain.numbers.{RegularNumber, StarNumber}
import io.koff.tickets.domain.tickets.{NormalTicket, SystemTicket}

class WinningClassSpec extends UnitSpec{
  "WinningClass" - {
    ".findWinningClass finds the coolest winning class available" in {
      val regular = regNums(1, 2, 3, 4, 5)
      val moreRegular =  regNums(6, 7, 8, 9, 10)

      val star = starNums(1, 2)
      val moreStar = starNums(3, 4, 5)

      val drawNumbers = Draw(
        regular = LimitedSet.unsafe(1, 5, regular),
        star    = LimitedSet.unsafe(1, 2, star)
      )
      val normalClass1 = NormalTicket.unsafe(regular, star)
      val systemClass1 = SystemTicket.unsafe(regular ++ moreRegular, star ++ moreStar)

      val normalClass5 = NormalTicket.unsafe(regNums(1, 2, 3, 4, 6), starNums(1, 3))
      val systemClass5 = SystemTicket.unsafe(
        regNums(1, 2, 3, 4, 6) ++ moreRegular,
        starNums(1, 3) ++ moreStar
      )

      val normalResult1 = WinningClass.findWinningClass(drawNumbers, normalClass1, WinningClass.All)
      normalResult1 mustBe Some(WinningClass.Class1)

      val systemResult1 = WinningClass.findWinningClass(drawNumbers, systemClass1, WinningClass.All)
      systemResult1 mustBe Some(WinningClass.Class1)

      val normalResult5 = WinningClass.findWinningClass(drawNumbers, normalClass5, WinningClass.All)
      normalResult5 mustBe Some(WinningClass.Class5)

      val systemResult5 = WinningClass.findWinningClass(drawNumbers, systemClass5, WinningClass.All)
      systemResult5 mustBe Some(WinningClass.Class5)
    }
  }

  private def regNums(values: Int*): Set[RegularNumber] = values.map(RegularNumber.unsafe).toSet
  private def starNums(values: Int*): Set[StarNumber] = values.map(StarNumber.unsafe).toSet
}
