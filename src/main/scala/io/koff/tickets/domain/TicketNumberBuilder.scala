package io.koff.tickets.domain

import io.circe.{Decoder, Encoder, Json}
import io.circe.DecodingFailure.fromThrowable

/**
  * Interface for a number companion object.
  * Contains logic of its validation, creation and parsing
  */
trait TicketNumberBuilder[T] {
  implicit def ticketNumber: TicketNumber[T]

  def minValue: Int
  def maxValue: Int

  def checkAndCreate(value: Int): Either[TicketNumberError, T] = {
    if(value < minValue) {
      Left(TooSmall(value, minValue))
    } else if (value > maxValue) {
      Left(TooBig(value, maxValue))
    } else {
      Right(create(value))
    }
  }

  def unsafe(value: Int): T = {
    checkAndCreate(value).fold(
      err => throw new IllegalArgumentException(s"problem while creating a ticket number: $err"),
      identity
    )
  }

  protected def create(value: Int): T

  implicit val TicketNumberOrdering: Ordering[T] = Ordering.by { number =>
    TicketNumber[T].value(number)
  }

  implicit val TicketNumberDecoder: Decoder[T] = c => for {
    value <- c.as[Int]
    ticketNumber <- checkAndCreate(value).left.map(fromThrowable(_, c.history))
  } yield {
    ticketNumber
  }

  implicit val TicketNumberEncoder: Encoder[T] = number => {
    Json.fromInt(TicketNumber[T].value(number))
  }
}

