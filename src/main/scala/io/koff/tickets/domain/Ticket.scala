package io.koff.tickets.domain

import io.koff.tickets.collections.LimitedSet
import io.koff.tickets.domain.numbers.{RegularNumber, StarNumber}

/**
  * Common interface for a ticket
  */
trait Ticket {
  /**
    * Regular numbers of a ticket
    */
  def regular: LimitedSet[RegularNumber]

  /**
    * Star numbers of a ticket
    */
  def star: LimitedSet[StarNumber]
}