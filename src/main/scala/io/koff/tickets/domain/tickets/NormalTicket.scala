package io.koff.tickets.domain.tickets

import io.koff.tickets.collections.LimitedSet
import io.koff.tickets.collections.LimitedSet.SetError
import io.koff.tickets.domain.numbers.{RegularNumber, StarNumber}
import io.koff.tickets.domain.{Ticket, TicketBuilder}

/**
  * Normal ticket.
  * See https://www.tipp24.com/euromillions
  * @param regular chosen regular numbers of the ticket
  * @param star chosen star numbers of the ticket
  */
case class NormalTicket private (
  override val regular: LimitedSet[RegularNumber],
  override val star:    LimitedSet[StarNumber]
) extends Ticket

object NormalTicket extends TicketBuilder[NormalTicket, NormalTicketError] {
  override val minRegular = 5
  override val maxRegular = 5
  override val minStar = 2
  override val maxStar = 2
  override val typeName: String = "Normal"


  override protected def regularErr(setErr: SetError): NormalTicketError = {
    NormalTicketError("problem while creating regular set", setErr)
  }

  override protected def starErr(setErr: SetError): NormalTicketError = {
    NormalTicketError("problem while creating star set", setErr)
  }

  override protected def create(
    regular: LimitedSet[RegularNumber],
    star: LimitedSet[StarNumber]
  ): NormalTicket = NormalTicket(regular, star)

}

case class NormalTicketError(msg: String, err: SetError) extends Exception(s"$msg: $err")
