name := "ticket_system"

version := "0.1"

scalaVersion := "2.12.6"

scalacOptions in ThisBuild ++= Seq("-Xfatal-warnings", "-Ywarn-unused-import", "-deprecation")

val circeVersion = "0.9.3"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser",
  "io.circe" %% "circe-generic-extras"
).map(_ % circeVersion)

libraryDependencies += "org.typelevel"      %% "cats-effect"       % "1.0.0-RC2"
libraryDependencies += "com.typesafe.akka"  %% "akka-stream-kafka" % "0.22"
libraryDependencies += "org.typelevel"      %% "cats-core"         % "1.1.0"
libraryDependencies += "org.scalatest"      %% "scalatest"         % "3.0.4"  % Test
libraryDependencies += "org.scalacheck"     %% "scalacheck"        % "1.14.0" % Test