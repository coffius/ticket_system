package io.koff.tickets.json

import io.circe._
import io.circe.syntax._
import io.koff.tickets.domain.Ticket
import io.koff.tickets.domain.tickets.{NormalTicket, SystemTicket}

/**
  * Circe encoders/decoders for tickets
  */
object TicketFormat {
  implicit val TicketEncoder: Encoder[Ticket] = ticket => {
    val ticketType = ticket match {
      case _: NormalTicket => NormalTicket.typeName
      case _: SystemTicket => SystemTicket.typeName
    }

    Json.obj(
      "$type"   -> Json.fromString(ticketType),
      "regular" -> ticket.regular.values.asJson,
      "star"    -> ticket.star.values.asJson
    )
  }

  implicit val TicketDecoder: Decoder[Ticket] = c => for {
    ticketType <- c.downField("$type").as[String]
    ticket <- decodeSpecificTicket(c, ticketType)
  } yield {
    ticket
  }

  private def decodeSpecificTicket(c: HCursor, ticketType: String): Decoder.Result[Ticket] = {
    ticketType match {
      case NormalTicket.typeName => NormalTicket.Decoder(c)
      case SystemTicket.typeName => SystemTicket.Decoder(c)
      case unknown =>
        val err = DecodingFailure.fromThrowable(
          new IllegalStateException(s"unknown ticket type: $unknown"),
          c.history
        )
        Left(err)
    }
  }
}
