package io.koff.tickets.domain

import io.koff.tickets.domain.numbers.RegularNumber
import io.koff.tickets.domain.numbers.RegularNumber._
import org.scalacheck._
import org.scalatest.prop.Checkers
import org.scalatest.{MustMatchers, PropSpec}

class RegularNumberSpec extends PropSpec with Checkers with MustMatchers {
  import Prop.forAll
  private val inRange = forAll(Gens.TicketNumberGens.correctInt(RegularNumber)) { value =>
    RegularNumber.checkAndCreate(value) == Right(RegularNumber(value))
  }

  private val tooSmall = forAll(Gens.TicketNumberGens.tooSmall(RegularNumber)) { value =>
    RegularNumber.checkAndCreate(value) == Left(TooSmall(value, minValue))
  }

  private val tooBig = forAll(Gens.TicketNumberGens.tooBig(RegularNumber)) { value =>
    RegularNumber.checkAndCreate(value) == Left(TooBig(value, maxValue))
  }

  property(s".from(...) is defined in range [$minValue, $maxValue]") {
    check(inRange)
  }

  property(s".from(...) is not defined in range [Int.MinValue, $minValue)") {
    check(tooSmall)
  }

  property(s".from(...) is not defined in range ($maxValue, Int.MinValue]") {
    check(tooBig)
  }
}
