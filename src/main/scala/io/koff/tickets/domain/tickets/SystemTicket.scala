package io.koff.tickets.domain.tickets

import io.koff.tickets.collections.LimitedSet
import io.koff.tickets.collections.LimitedSet.SetError
import io.koff.tickets.domain.numbers.{RegularNumber, StarNumber}
import io.koff.tickets.domain.{Ticket, TicketBuilder}

/**
  * System ticket.
  * See https://www.tipp24.com/euromillions/systemschein/
  * @param regular chosen regular numbers of the ticket
  * @param star chose star numbers of the ticket
  */
case class SystemTicket private (
  override val regular: LimitedSet[RegularNumber],
  override val star: LimitedSet[StarNumber]
) extends Ticket

object SystemTicket extends TicketBuilder[SystemTicket, SystemTicketError] {
  override val minRegular = 5
  override val maxRegular = 10
  override val minStar = 3
  override val maxStar = 5
  override val typeName: String = "System"

  override protected def regularErr(setErr: SetError): SystemTicketError = {
    SystemTicketError("problem while creating regular set", setErr)
  }

  override protected def starErr(setErr: SetError): SystemTicketError = {
    SystemTicketError("problem while creating star set", setErr)
  }

  override protected def create(
    regular: LimitedSet[RegularNumber],
    star: LimitedSet[StarNumber]
  ): SystemTicket = SystemTicket(regular, star)
}

case class SystemTicketError(msg: String, err: SetError) extends Exception(s"$msg: $err")
