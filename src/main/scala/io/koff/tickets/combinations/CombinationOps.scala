package io.koff.tickets.combinations

import scala.collection.SortedSet

object CombinationOps {
  /**
    * Finds all possible combinations of subsets of the provided sets
    * @param input1 the first set
    * @param len1 the size of subsets for the first set
    * @param input2 the second set
    * @param len2 the size of subsets for the second set
    * @return a lazy stream of all combinations
    */
  def cartesianCombinations[T](
    input1: SortedSet[T],
    len1: Int,
    input2: SortedSet[T],
    len2: Int
  ): Stream[(SortedSet[T], SortedSet[T])] = {
    val stream1 = input1.subsets(len1).toStream
    val stream2 = input2.subsets(len2).toStream
    for {
      s1 <- stream1
      s2 <- stream2
    } yield {
      (s1, s2)
    }
  }
}
