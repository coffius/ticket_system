package io.koff.tickets.domain.numbers

import io.koff.tickets.domain.{TicketNumber, TicketNumberBuilder}

/**
  * Value class for regular numbers in tickets
  */
case class RegularNumber private (value: Int) extends AnyVal

object RegularNumber extends TicketNumberBuilder[RegularNumber] {
  override val minValue: Int = 1
  override val maxValue: Int = 50
  override protected def create(value: Int): RegularNumber = RegularNumber(value)
  override implicit val ticketNumber: TicketNumber[RegularNumber] = ticketNumber => ticketNumber.value
}
