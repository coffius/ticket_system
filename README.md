# Ticket System

There are 3 runnable classes in the code base:

1. `io.koff.tickets.main.Task2` - calculates all combinations for a ticket
1. `io.koff.tickets.main.Task3` - calculates a number of won tickets per winning class
1. `io.koff.tickets.domain.TicketGenerator` - generates 1000 of random tickets and saves them to a file.