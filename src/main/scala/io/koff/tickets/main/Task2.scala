package io.koff.tickets.main

import cats.effect.{ExitCode, IO, IOApp}
import cats.instances.string._
import cats.syntax.show._
import io.koff.tickets.domain.Combination
import io.koff.tickets.json.TicketFormat._
import io.koff.tickets.main.Common._

/**
  * Prints all possible winning combinations for a ticket
  */
object Task2 extends IOApp {
  private val Filename: String = "task_2.json"

  def run(args: List[String]): IO[ExitCode] = for {
    ticket <- readObjFromFile(Filename)
    combinations = Combination.allCombinations(ticket)
    printable = combinations.zipWithIndex.map {
      case (comb, index) => s"Comb #${index + 1}: ${comb.show}"
    }
    _ <- Common.printAll(printable)
  } yield {
    ExitCode.Success
  }
}
