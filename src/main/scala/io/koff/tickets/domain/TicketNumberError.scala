package io.koff.tickets.domain

/**
  * Hierarchy of validation errors for a ticket number
  */
sealed trait TicketNumberError extends Exception
case class TooSmall(current: Int, min: Int) extends TicketNumberError
case class TooBig(current: Int, max: Int) extends TicketNumberError

