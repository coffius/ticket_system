package io.koff.tickets.domain

import io.circe.Decoder
import io.circe.DecodingFailure.fromThrowable
import io.koff.tickets.collections.LimitedSet
import io.koff.tickets.collections.LimitedSet.SetError
import io.koff.tickets.domain.numbers.{RegularNumber, StarNumber}

import scala.collection.{Set, SortedSet}

/**
  * Interface of a ticket companion object.
  * Contains common logic of validation, creation and parsing.
  */
trait TicketBuilder[T <: Ticket, Err <: Throwable] {
  def minRegular: Int
  def maxRegular: Int
  def minStar: Int
  def maxStar: Int
  def typeName: String

  protected def create(regular: LimitedSet[RegularNumber], star: LimitedSet[StarNumber]): T
  protected def regularErr(err: SetError): Err
  protected def starErr(err: SetError): Err

  def checkAndCreate(
    regular: Set[RegularNumber],
    star: Set[StarNumber]
  ): Either[Err, T] = for {
    regularSet <- LimitedSet.build(minRegular, maxRegular, regular).left.map(regularErr)
    starSet <- LimitedSet.build(minStar, maxStar, star).left.map(starErr)
  } yield {
    create(regularSet, starSet)
  }

  def unsafe(regular: Set[RegularNumber], star: Set[StarNumber]): T = {
    checkAndCreate(regular, star).fold(
      err => throw new IllegalArgumentException(s"problem while creating a ticket: $err"),
      identity
    )
  }

  implicit val Decoder: Decoder[T] = c => {
    for {
      regular <- c.downField("regular").as[SortedSet[RegularNumber]]
      star <- c.downField("star").as[SortedSet[StarNumber]]
      ticket <- checkAndCreate(regular, star).left.map(fromThrowable(_, c.history))
    } yield {
      ticket
    }
  }
}