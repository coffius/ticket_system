package io.koff.tickets.collections

import scala.collection.{Set, SortedSet}

/**
  * Set with lower and upper boundaries for its length
  */
case class LimitedSet[T] private (minSize: Int, maxSize: Int, values: SortedSet[T])

object LimitedSet {
  sealed trait SetError
  case class TooLong (maxSize: Int, currentSize: Int) extends SetError
  case class TooShort(minSize: Int, currentSize: Int) extends SetError

  /**
    * Builds and returns a new instance of LimitedSet or an error if `values` don't comply with preconditions.
    */
  def build[T: Ordering](minSize: Int, maxSize: Int, values: Set[T]): Either[SetError, LimitedSet[T]] = {
    if(values.size < minSize) {
      Left(TooShort(minSize, values.size))
    } else if(values.size > maxSize) {
      Left(TooLong(maxSize, values.size))
    } else {
      Right(LimitedSet(minSize, maxSize, SortedSet.empty ++ values))
    }
  }

  def unsafe[T: Ordering](minSize: Int, maxSize: Int, values: Set[T]): LimitedSet[T] = {
    build(minSize, maxSize, values).fold(
      err => throw new IllegalArgumentException(s"problem while creating LimitedSet: $err"),
      identity
    )
  }
}