package io.koff.tickets.domain

/**
  * Type class that extracts a value from a number implementation
  */
trait TicketNumber[T] {
  def value(ticketNumber: T): Int
}

object TicketNumber {
  def apply[T: TicketNumber]: TicketNumber[T] = implicitly[TicketNumber[T]]
}