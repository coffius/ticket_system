package io.koff.tickets.domain

import io.koff.tickets.domain.numbers.{RegularNumber, StarNumber}
import io.koff.tickets.domain.tickets.{NormalTicket, SystemTicket}
import org.scalacheck.Gen

import scala.util.Random

object Gens {
  object Utils {
    def setOfSize[T](min: Int, max: Int, gen: Gen[T]): Gen[Set[T]] = {
      val len = max - min
      require(len >= 0, s"max[$max] should be greater or equal to min[$min]")
      for {
        minColl <- Gen.listOfN(min, gen).map(_.toSet).retryUntil(_.size == min)
        addColl <- Gen.listOfN(len, gen).map(_.toSet).retryUntil(_.size == len)
      } yield {
        minColl ++ addColl
      }
    }

    def minMax(left: Int, right: Int): Gen[(Int, Int)] = for {
      value1 <- Gen.chooseNum(left, right)
      value2 <- Gen.chooseNum(left, right).retryUntil(_ != value1)
      min = Math.min(value1, value2)
      max = Math.max(value1, value2)
    } yield {
      (min, max)
    }
  }
  object TicketNumberGens {
    def correctInt[T](bld: TicketNumberBuilder[T]): Gen[Int] = {
      Gen.chooseNum(bld.minValue, bld.maxValue)
    }

    def correctTicketNumber[T](bld: TicketNumberBuilder[T]): Gen[T] = for {
      intValue <- correctInt(bld)
    } yield {
      bld
        .checkAndCreate(intValue)
        .right
        .getOrElse(throw new IllegalStateException(s"invalid value[$intValue] has been generated"))
    }

    def tooSmall[T](bld: TicketNumberBuilder[T]): Gen[Int] = {
      Gen.chooseNum(Int.MinValue, bld.minValue - 1)
    }
    def tooBig[T](bld: TicketNumberBuilder[T]): Gen[Int] = {
      Gen.chooseNum(bld.maxValue + 1, Int.MaxValue)
    }
  }

  object LimitedSetGens {
    val rng: Gen[Int] = Gen.negNum[Int].flatMap(_ => Random.nextInt())

    def correct[T](gen: Gen[T]): Gen[(Int, Int, Set[T])] = for {
      (min, max) <- Utils.minMax(1, 1000)
      coll <- Utils.setOfSize(min, max, gen)
    } yield {
      (min, max, coll)
    }

    def lessThanNeeded[T](gen: Gen[T]): Gen[(Int, Int, Set[T])] = for {
      (min, max) <- Utils.minMax(3, 1000)
      coll <- Utils.setOfSize(1, min - 1, gen)
    } yield {
      (min, max, coll)
    }

    def moreThanNeeded[T](gen: Gen[T]): Gen[(Int, Int, Set[T])] = for {
      (min, max) <- Utils.minMax(2, 1000)
      coll <- Utils.setOfSize(max + 1, max + 1000, gen)
    } yield {
      (min, max, coll)
    }
  }

  object TicketGens {
    def correctByBuilder[T <: Ticket](bld: TicketBuilder[T, _]): Gen[T] = {
      val regularGen = TicketNumberGens.correctTicketNumber(RegularNumber)
      val starGen = TicketNumberGens.correctTicketNumber(StarNumber)
      for {
        regular <- Utils.setOfSize(bld.minRegular, bld.maxRegular, regularGen)
        star <- Utils.setOfSize(bld.minStar, bld.maxStar, starGen)
      } yield {
        bld
          .checkAndCreate(regular, star)
          .right
          .getOrElse(throw new IllegalStateException(s"invalid values[$regular, $star] have been generated"))
      }
    }

    def correctTicket: Gen[Ticket] = {
      val normalGen = Gens.TicketGens.correctByBuilder(NormalTicket)
      val systemGen = Gens.TicketGens.correctByBuilder(SystemTicket)

      Gen.frequency(1 -> normalGen, 1 -> systemGen)
    }
  }
}
