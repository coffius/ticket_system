package io.koff.tickets.collections

import io.koff.tickets.collections.LimitedSet.{TooLong, TooShort}
import io.koff.tickets.domain.Gens
import org.scalacheck.Prop
import org.scalatest.prop.Checkers
import org.scalatest.{MustMatchers, PropSpec}

import scala.collection.SortedSet

class LimitedSetSpec extends PropSpec with Checkers with MustMatchers {
  import Prop.forAllNoShrink

  private val correct = forAllNoShrink(Gens.LimitedSetGens.correct(Gens.LimitedSetGens.rng)) {
    case (min, max, values) =>
      LimitedSet.build(min, max, values) == Right(LimitedSet(min, max, SortedSet.empty ++ values))
  }

  private val lessThanNeeded = forAllNoShrink(Gens.LimitedSetGens.lessThanNeeded(Gens.LimitedSetGens.rng)) {
    case (min, max, values) =>
      LimitedSet.build(min, max, values) == Left(TooShort(min, values.size))
  }

  private val moreThanNeeded = forAllNoShrink(Gens.LimitedSetGens.moreThanNeeded(Gens.LimitedSetGens.rng)) {
    case (min, max, values) =>
      LimitedSet.build(min, max, values) == Left(TooLong(max, values.size))
  }


  property(s"build(...) returns Rigth(LimitedSet(...)) for length between [min, max]") {
    check(correct)
  }

  property(s"build(...) returns Left(TooShort(...)) for length less than [min, ...)") {
    check(lessThanNeeded)
  }

  property(s"build(...) returns Left(TooLong(...)) for length more than (..., max]") {
    check(moreThanNeeded)
  }
}
