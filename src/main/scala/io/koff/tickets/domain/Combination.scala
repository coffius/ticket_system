package io.koff.tickets.domain

import cats.Show
import io.koff.tickets.combinations.CombinationOps

import scala.collection.SortedSet

/**
  * One of possible combination of numbers for a ticket
  */
case class Combination(regular: SortedSet[Int], star: SortedSet[Int])

object Combination {
  private val RegularElems = 5
  private val StarElems = 2

  /**
    * Returns a stream of all possible combinations of win numbers to the ticket
    */
  def allCombinations(ticket: Ticket): Stream[Combination] = {
    CombinationOps.cartesianCombinations(
      ticket.regular.values.map(_.value),
      RegularElems,
      ticket.star.values.map(_.value),
      StarElems
    ).map((Combination.apply _).tupled)
  }

  implicit val CombinationShow: Show[Combination] = Show.show { comb =>
    val regularString = comb.regular.mkString(", ")
    val starString = comb.star.mkString(", ")
    s"[$regularString | $starString]"
  }
}
